#-*- coding: UTF-8 -*-
from itertools import izip_longest
import re
import json
import urllib2

from django import template
from django.conf import settings


register = template.Library()
@register.filter('get_flam_user')
def get_flam_user(value):
    req = urllib2.Request(
        value + ('?access_token=%s' % settings.FLAMP_ACCESS_TOKEN)
    )
    return json.loads(urllib2.urlopen(req).read())

@register.filter('get_dict_value')
def get_dict_value(value, arg):
    v = value
    for i in arg.split('.'):
        v = v[i]
    return v

@register.filter('format_flamp_img')
def format_flamp_img(value):
    regex = re.compile("(.+)\.(.+)")
    r = regex.search(value).groups()
    return r[0] + '_100_100.' + r[1]

@register.filter('delta')
def delta(value, arg):
    return value - arg

"""
@register.inclusion_tag('wheretogonext/get_flam_user.html')
def get_flam_user():
    return {}
"""