#-*- coding: UTF-8 -*-
import urllib2, os
import pytils
from datetime import datetime
from django.conf import settings
from easy_thumbnails.files import get_thumbnailer

def get_image_from_place(place):
    # Сохраняем картинку с сервера
    if place.get_main_image():
        img = place.get_main_image()

        # Проверяем, есть ли картинка
        if not os.path.exists(img.get_local_filepath()):
            # Скачиваем её
            file = urllib2.urlopen(str(img.image)).read()
            f = open(os.path.join(settings.MEDIA_ROOT, img.get_local_filepath()), "wb")
            f.write(file)
            f.close()

            img.image = img.get_local_filepath()
            img.save()

        return get_thumbnailer(img.get_local_filepath()).get_thumbnail({
                'size': (400, 400),
                'crop': True
            }).url
    else:
        return None