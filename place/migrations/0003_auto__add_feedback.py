# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Feedback'
        db.create_table(u'place_feedback', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('content', self.gf('django.db.models.fields.TextField')(max_length=100)),
        ))
        db.send_create_signal(u'place', ['Feedback'])


    def backwards(self, orm):
        # Deleting model 'Feedback'
        db.delete_table(u'place_feedback')


    models = {
        u'place.category': {
            'Meta': {'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        },
        u'place.categoryrelation': {
            'Meta': {'object_name': 'CategoryRelation'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['place.Category']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['place.Place']"})
        },
        u'place.contact': {
            'Meta': {'object_name': 'Contact'},
            'contact_type': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'place_item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'contacts'", 'to': u"orm['place.Place']"}),
            'value': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        },
        u'place.doublegiscategory': {
            'Meta': {'object_name': 'DoubleGisCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'place.doublegistag': {
            'Meta': {'object_name': 'DoubleGisTag'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'place.feedback': {
            'Meta': {'object_name': 'Feedback'},
            'content': ('django.db.models.fields.TextField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'place.itemimage': {
            'Meta': {'object_name': 'ItemImage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'is_main': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'place_item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['place.Place']"})
        },
        u'place.menuprice': {
            'Meta': {'object_name': 'MenuPrice'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'menu_item': ('django.db.models.fields.CharField', [], {'default': "'bear'", 'max_length': '5'}),
            'place_item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'menus'", 'to': u"orm['place.Place']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '10', 'decimal_places': '2'})
        },
        u'place.place': {
            'Meta': {'object_name': 'Place'},
            'addon': ('django.db.models.fields.TextField', [], {'default': "''", 'max_length': '2000', 'blank': 'True'}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'avr_price': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'default': "''", 'max_length': '140', 'blank': 'True'}),
            'double_gis_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'draft': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'enter_price_man': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'enter_price_woman': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'facebook_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'flamp_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'foursquare_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'geo_coordinate_lat': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'geo_coordinate_lng': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_category': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'maincategory_place_set'", 'symmetrical': 'False', 'through': u"orm['place.CategoryRelation']", 'to': u"orm['place.Category']"}),
            'option_category': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'option_place_set'", 'blank': 'True', 'to': u"orm['place.Category']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'twitter_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'vk_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'with_who': ('django.db.models.fields.CharField', [], {'default': "'any'", 'max_length': '5', 'blank': 'True'})
        },
        u'place.placemeta': {
            'Meta': {'object_name': 'PlaceMeta'},
            'double_gis_category': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['place.DoubleGisCategory']", 'symmetrical': 'False', 'blank': 'True'}),
            'double_gis_tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['place.DoubleGisTag']", 'symmetrical': 'False', 'blank': 'True'}),
            'flamp_rate': ('django.db.models.fields.FloatField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'flamp_recommendation_count': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'flamp_review_count': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'place': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['place.Place']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'place.worktime': {
            'Meta': {'object_name': 'WorkTime'},
            'day': ('django.db.models.fields.CharField', [], {'default': "'any'", 'max_length': '3'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'place_item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'workedtimes'", 'to': u"orm['place.Place']"}),
            'time_from': ('django.db.models.fields.TimeField', [], {'default': "''", 'max_length': '20'}),
            'time_to': ('django.db.models.fields.TimeField', [], {'default': "''", 'max_length': '20'})
        }
    }

    complete_apps = ['place']