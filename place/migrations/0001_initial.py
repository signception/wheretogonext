# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Category'
        db.create_table(u'place_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=100)),
        ))
        db.send_create_signal(u'place', ['Category'])

        # Adding model 'Place'
        db.create_table(u'place_place', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('draft', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('description', self.gf('django.db.models.fields.TextField')(default='', max_length=140, blank=True)),
            ('addon', self.gf('django.db.models.fields.TextField')(default='', max_length=2000, blank=True)),
            ('with_who', self.gf('django.db.models.fields.CharField')(default='any', max_length=5, blank=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=150, blank=True)),
            ('geo_coordinate_lng', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('geo_coordinate_lat', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('avr_price', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=10, decimal_places=2, blank=True)),
            ('enter_price_man', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=10, decimal_places=2, blank=True)),
            ('enter_price_woman', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=10, decimal_places=2, blank=True)),
            ('vk_id', self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True)),
            ('twitter_id', self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True)),
            ('facebook_id', self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True)),
            ('flamp_id', self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True)),
            ('foursquare_id', self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True)),
            ('double_gis_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
        ))
        db.send_create_signal(u'place', ['Place'])

        # Adding M2M table for field option_category on 'Place'
        m2m_table_name = db.shorten_name(u'place_place_option_category')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('place', models.ForeignKey(orm[u'place.place'], null=False)),
            ('category', models.ForeignKey(orm[u'place.category'], null=False))
        ))
        db.create_unique(m2m_table_name, ['place_id', 'category_id'])

        # Adding model 'DoubleGisTag'
        db.create_table(u'place_doublegistag', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'place', ['DoubleGisTag'])

        # Adding model 'DoubleGisCategory'
        db.create_table(u'place_doublegiscategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'place', ['DoubleGisCategory'])

        # Adding model 'PlaceMeta'
        db.create_table(u'place_placemeta', (
            ('flamp_rate', self.gf('django.db.models.fields.FloatField')(default=None, null=True, blank=True)),
            ('flamp_review_count', self.gf('django.db.models.fields.IntegerField')(default=None, null=True, blank=True)),
            ('flamp_recommendation_count', self.gf('django.db.models.fields.IntegerField')(default=None, null=True, blank=True)),
            ('place', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['place.Place'], unique=True, primary_key=True)),
        ))
        db.send_create_signal(u'place', ['PlaceMeta'])

        # Adding M2M table for field double_gis_tags on 'PlaceMeta'
        m2m_table_name = db.shorten_name(u'place_placemeta_double_gis_tags')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('placemeta', models.ForeignKey(orm[u'place.placemeta'], null=False)),
            ('doublegistag', models.ForeignKey(orm[u'place.doublegistag'], null=False))
        ))
        db.create_unique(m2m_table_name, ['placemeta_id', 'doublegistag_id'])

        # Adding M2M table for field double_gis_category on 'PlaceMeta'
        m2m_table_name = db.shorten_name(u'place_placemeta_double_gis_category')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('placemeta', models.ForeignKey(orm[u'place.placemeta'], null=False)),
            ('doublegiscategory', models.ForeignKey(orm[u'place.doublegiscategory'], null=False))
        ))
        db.create_unique(m2m_table_name, ['placemeta_id', 'doublegiscategory_id'])

        # Adding model 'CategoryRelation'
        db.create_table(u'place_categoryrelation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('place', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['place.Place'])),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['place.Category'])),
            ('order', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'place', ['CategoryRelation'])

        # Adding model 'ItemImage'
        db.create_table(u'place_itemimage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('place_item', self.gf('django.db.models.fields.related.ForeignKey')(related_name='images', to=orm['place.Place'])),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal(u'place', ['ItemImage'])

        # Adding model 'WorkTime'
        db.create_table(u'place_worktime', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('place_item', self.gf('django.db.models.fields.related.ForeignKey')(related_name='workedtimes', to=orm['place.Place'])),
            ('day', self.gf('django.db.models.fields.CharField')(default='any', max_length=3)),
            ('time_from', self.gf('django.db.models.fields.TimeField')(default='', max_length=20)),
            ('time_to', self.gf('django.db.models.fields.TimeField')(default='', max_length=20)),
        ))
        db.send_create_signal(u'place', ['WorkTime'])

        # Adding model 'Contact'
        db.create_table(u'place_contact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('place_item', self.gf('django.db.models.fields.related.ForeignKey')(related_name='contacts', to=orm['place.Place'])),
            ('contact_type', self.gf('django.db.models.fields.CharField')(default=None, max_length=100)),
            ('value', self.gf('django.db.models.fields.CharField')(default='', max_length=100)),
        ))
        db.send_create_signal(u'place', ['Contact'])

        # Adding model 'MenuPrice'
        db.create_table(u'place_menuprice', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('place_item', self.gf('django.db.models.fields.related.ForeignKey')(related_name='menus', to=orm['place.Place'])),
            ('menu_item', self.gf('django.db.models.fields.CharField')(default='bear', max_length=5)),
            ('price', self.gf('django.db.models.fields.DecimalField')(default=0, max_digits=10, decimal_places=2)),
        ))
        db.send_create_signal(u'place', ['MenuPrice'])


    def backwards(self, orm):
        # Deleting model 'Category'
        db.delete_table(u'place_category')

        # Deleting model 'Place'
        db.delete_table(u'place_place')

        # Removing M2M table for field option_category on 'Place'
        db.delete_table(db.shorten_name(u'place_place_option_category'))

        # Deleting model 'DoubleGisTag'
        db.delete_table(u'place_doublegistag')

        # Deleting model 'DoubleGisCategory'
        db.delete_table(u'place_doublegiscategory')

        # Deleting model 'PlaceMeta'
        db.delete_table(u'place_placemeta')

        # Removing M2M table for field double_gis_tags on 'PlaceMeta'
        db.delete_table(db.shorten_name(u'place_placemeta_double_gis_tags'))

        # Removing M2M table for field double_gis_category on 'PlaceMeta'
        db.delete_table(db.shorten_name(u'place_placemeta_double_gis_category'))

        # Deleting model 'CategoryRelation'
        db.delete_table(u'place_categoryrelation')

        # Deleting model 'ItemImage'
        db.delete_table(u'place_itemimage')

        # Deleting model 'WorkTime'
        db.delete_table(u'place_worktime')

        # Deleting model 'Contact'
        db.delete_table(u'place_contact')

        # Deleting model 'MenuPrice'
        db.delete_table(u'place_menuprice')


    models = {
        u'place.category': {
            'Meta': {'object_name': 'Category'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        },
        u'place.categoryrelation': {
            'Meta': {'object_name': 'CategoryRelation'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['place.Category']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'order': ('django.db.models.fields.IntegerField', [], {}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['place.Place']"})
        },
        u'place.contact': {
            'Meta': {'object_name': 'Contact'},
            'contact_type': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'place_item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'contacts'", 'to': u"orm['place.Place']"}),
            'value': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        },
        u'place.doublegiscategory': {
            'Meta': {'object_name': 'DoubleGisCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'place.doublegistag': {
            'Meta': {'object_name': 'DoubleGisTag'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'place.itemimage': {
            'Meta': {'object_name': 'ItemImage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'place_item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'images'", 'to': u"orm['place.Place']"})
        },
        u'place.menuprice': {
            'Meta': {'object_name': 'MenuPrice'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'menu_item': ('django.db.models.fields.CharField', [], {'default': "'bear'", 'max_length': '5'}),
            'place_item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'menus'", 'to': u"orm['place.Place']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '10', 'decimal_places': '2'})
        },
        u'place.place': {
            'Meta': {'object_name': 'Place'},
            'addon': ('django.db.models.fields.TextField', [], {'default': "''", 'max_length': '2000', 'blank': 'True'}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'avr_price': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'default': "''", 'max_length': '140', 'blank': 'True'}),
            'double_gis_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'draft': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'enter_price_man': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'enter_price_woman': ('django.db.models.fields.DecimalField', [], {'default': '0', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'facebook_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'flamp_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'foursquare_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'geo_coordinate_lat': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'geo_coordinate_lng': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'main_category': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'maincategory_place_set'", 'symmetrical': 'False', 'through': u"orm['place.CategoryRelation']", 'to': u"orm['place.Category']"}),
            'option_category': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'option_place_set'", 'blank': 'True', 'to': u"orm['place.Category']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'twitter_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'vk_id': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100', 'blank': 'True'}),
            'with_who': ('django.db.models.fields.CharField', [], {'default': "'any'", 'max_length': '5', 'blank': 'True'})
        },
        u'place.placemeta': {
            'Meta': {'object_name': 'PlaceMeta'},
            'double_gis_category': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['place.DoubleGisCategory']", 'symmetrical': 'False', 'blank': 'True'}),
            'double_gis_tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['place.DoubleGisTag']", 'symmetrical': 'False', 'blank': 'True'}),
            'flamp_rate': ('django.db.models.fields.FloatField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'flamp_recommendation_count': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'flamp_review_count': ('django.db.models.fields.IntegerField', [], {'default': 'None', 'null': 'True', 'blank': 'True'}),
            'place': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['place.Place']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'place.worktime': {
            'Meta': {'object_name': 'WorkTime'},
            'day': ('django.db.models.fields.CharField', [], {'default': "'any'", 'max_length': '3'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'place_item': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'workedtimes'", 'to': u"orm['place.Place']"}),
            'time_from': ('django.db.models.fields.TimeField', [], {'default': "''", 'max_length': '20'}),
            'time_to': ('django.db.models.fields.TimeField', [], {'default': "''", 'max_length': '20'})
        }
    }

    complete_apps = ['place']