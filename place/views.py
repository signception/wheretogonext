#-*- coding: UTF-8 -*-
import random
import urllib2
from django.core.mail import send_mail
from django.template.loader import render_to_string
from geopy.distance import distance as geopy_distance

from django.http.response import HttpResponseRedirect, HttpResponse, Http404
from django.views.generic.detail import DetailView
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404

from .models import *
from place.taxi import TaxiRequest
from place.forms import CallTaxi, FeedbackForm
# Create your views here.


MY_POINT = [
    56.831718, # Широта, latitude
    60.602092, # Долгота, longitude
]


def index(request):
    return render(request,
          'place/index.html', {})

def company(request):
    if request.GET.get('company', None) is not None:
        comp = request.GET['company']
        if dict(WITH_WHO).get(comp, None) is not None:
            request.session['company'] = comp
            return redirect(reverse('select_category'))

    return render(request,
          'place/company.html', {})

def category(request):
    if request.GET.get('cat', None) is not None:
        cat = request.GET['cat']

        try:
            if Category.objects.get(pk=cat):
                request.session['category'] = cat
                return redirect(reverse('get_venues'))
        except (Category.DoesNotExist):
            pass

    return render(request,
          'place/category.html', {
                'categories': Category.objects.all()
            })

def get_venues(request):
    if not request.session.get('category', None):
        return redirect('select_company')

    venue = Place.objects.filter(
        #main_category=request.session['category']
        placemeta__double_gis_category=request.session['category']
    ).order_by('?')[0]

    dst = geopy_distance(
            MY_POINT,
            [float(venue.geo_coordinate_lat), float(venue.geo_coordinate_lng)]
        )

    return render(request,
          'place/get_venues.html', {
            'venue': venue,
            'distance': round(dst.meters / 1000, 2),
            })

class PlaceItemDetailView(DetailView):
    model = Place
    context_object_name = 'venue'

    def get_context_data(self, **kwargs):
        context = super(PlaceItemDetailView, self).get_context_data(**kwargs)
        to_address = {
            'street': context['object'].get_street(),
            'house': context['object'].get_house(),
        }
        taxi = TaxiRequest()
        taxi.calculate_cost(MY_POINT, to_address)

        context['taxi'] = taxi
        return context


def get_taxi(request, pk, uid=None):
    obj = get_object_or_404(Place, pk=pk)
    to_address = {
        'street': obj.get_street(),
        'house': obj.get_house(),
    }

    taxi = TaxiRequest()

    if request.method == 'POST':
        call = taxi.request(
            uid,
            request.POST['phone'],
            request.POST['captcha']
        )
        if call is True:
            return redirect(reverse('taxi_success', args=[obj.pk]))
        else:
            messages.add_message(request, messages.ERROR, call)
            return redirect(reverse('taxi_failure', args=[obj.pk]))

    taxi.calculate_cost(MY_POINT, to_address)

    form = CallTaxi()

    return render(request,
          'place/get_taxi.html', {
                'taxi': taxi,
                'venue': obj,
                'form': form,
                'uid': str(random.random())
            })

def taxi_success(request, pk):
    obj = get_object_or_404(Place, pk=pk)

    if request.method == 'POST':
        feedback_form = FeedbackForm(request.POST)
        if feedback_form.is_valid():
            feedback = feedback_form.save()
            
            send_mail(
                u'Отзыв с сервиса «Where to next go»',
                render_to_string(
                    'place/email_ask_form.txt',
                    { 
                        'feedback': feedback, 
                        'url': request.build_absolute_uri(reverse('admin:place_feedback_change', args=[feedback.pk]))
                    }
                ), 'noreply@wheretonextgo.com', [ 'info@signception.ru' ]
            )
            messages.add_message(request, messages.SUCCESS, 'Сообщение было успешно отправлено, спасибо!')

    else:
        feedback_form = FeedbackForm()

    return render(request,
          'place/call_taxi_success.html', {
                'venue': obj,
                'feedback_form': feedback_form
            })

def taxi_failure(request, pk):
    obj = get_object_or_404(Place, pk=pk)
    return render(request,
          'place/call_taxi_failure.html', {
                'venue': obj,
            })

def captcha(request, phpsessionid):
    taxi_url = 'http://www.300-01-01.ru/'
    headers = {
        'User-Agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.77 Safari/537.36',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Encoding': 'gzip,deflate,sdch',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Cookie': 'PHPSESSID=%s' % phpsessionid,
        'Origin': 'www.300-01-01.ru',
        'Pragma': 'no-cache',
    }

    url = str('http://www.300-01-01.ru/order/kcaptcha/index.php?PHPSESSID=%s' % phpsessionid)
    req = urllib2.Request(url, '', headers=headers)
    captcha = urllib2.urlopen(req)

    response = HttpResponse(captcha.read(), mimetype="image/jpeg")
    return response