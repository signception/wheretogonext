#-*- coding: UTF-8 -*-

import json
import urllib
import urllib2


class TaxiRequest(object):

    taxi_url = 'http://ekt.rutaxi.ru/'
    street_url = 'http://ekt.rutaxi.ru/ajax_street.html?type=1&term=%s'
    order_url = 'http://ekt.rutaxi.ru/ajax_order.html'
    googlemap_url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&sensor=false&language=ru'
    cookie = {}

    def __init__(self, from_coord, to_address, **kwargs):
        self.from_a = self._get_street(self.get_address_from_coord(from_coord))
        self.to_a = self._get_street(to_address)

        self.from_h = self.get_address_from_coord(from_coord)['house'].encode('utf-8')
        self.to_h =  to_address['house'].encode('utf-8')

        params = self._get_request_params('0')
        data = self._get_json(self.order_url, params, 'POST')

        self.price = data['cost']

    def request(self):
        p = self._get_request_params('2')
        data = self._get_json(self.order_url, p, 'POST')
        try:
            return data['oid']
        except (IndexError):
            return None

    def get_address_from_coord(self, coord):
        url = self.googlemap_url % (coord[0], coord[1])
        data = self._get_json(url)
        a = data['results'][0]['address_components']

        return {
            'street': a[1]['short_name'].replace(u'ул. ', ''),
            'house': a[0]['short_name'],
        }

    def _get_json(self, url, data=[], method='GET'):
        header = { 'User-Agent' : 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)' }
        header.update(self._get_cookie())

        data = urllib.urlencode(data)
        req = urllib2.Request(url, data, header)
        req.get_method = lambda: method
        req = urllib2.urlopen(req)

        self._set_cookie(req)
        return json.loads(req.read())
    
    def _get_street(self, text):
        street = text['street'].encode('utf-8').replace(' ', '')
        street_list = self._get_json(self.street_url % street)
        try:
            return street_list[0]['id'].replace('|', '-')
        except (IndexError): return None

    def _get_request_params(self, step):
        params = (
            ('p[]', step),
            ('p[]', '0'),
            ('p[]', ''),
            ('p[]', ''),
            ('p[]', '0'),
            ('p[]', '1'),
            ('p[]', ''),
            ('p[]', ''),
            ('p[]', ''),
            ('p[]', ''),
            ('p[]', self.from_a), # id улицы, с которой едем
            ('p[]', self.from_h), # Номер дома
            ('p[]', ''),
            ('p[]', ''),
            ('p[]', '%s#$%s' % (self.to_a, self.to_h)), # id_улицы#$номер_дома
            ('sms', '0'),
            ('iphone', ''),
            ('predvtsms', ''),
            ('java', '0')
        )
        return params

    def _set_cookie(self, request=None):
        if request is not None:
            if self.taxi_url in request.geturl():
                c = request.info().get('Set-Cookie')
                if c is not None:
                    c = c.split('; ')
                    for i in c:
                        if self.cookie.get(i.split('=')[0], None) is None:
                            self.cookie.update({
                                i.split('=')[0]: i.split('=')[1]
                            })
        return self.cookie

    def _get_cookie(self):
        if self.cookie:
            c = []
            for i in self.cookie.keys():
                c.append(
                   '%s=%s' % (i, self.cookie[i])
                )
            return {'Cookie': '; '.join(c)}
        return {'Cookie': ''}