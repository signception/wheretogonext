#-*- coding: UTF-8 -*-
from django import forms
from .models import Feedback


class CallTaxi(forms.Form):

    phone = forms.CharField(
        max_length=200,
        #initial='89043815963',
        widget=forms.TextInput(
            attrs={'size': 15}
        ), label=u'Телефон')
    captcha = forms.CharField(
        max_length=200,
        widget=forms.TextInput(
            attrs={'size': 10}
        ), label=u'Защитный код')

class FeedbackForm(forms.ModelForm):
    content = forms.CharField(
        widget=forms.Textarea(
            attrs={'placeholder': u'Напиши тут отзыв о работе с нашим сервисом, это для нас очень важно. Тогда мы сможем сделать еще лучше.'}
        ), label=u'Отзыв')
    class Meta:
        model = Feedback