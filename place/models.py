#-*- coding: UTF-8 -*-
import json
import os
import urllib2
import pytils
from urllib import urlretrieve
from datetime import datetime, timedelta

from geopy.distance import distance as geopy_distance

from django.conf import settings
from django.db import models

#  Хэлпер для генерации правильного пути к файлам
def _create_file_path(_, filename):
    now = datetime.now()
    date_path = now.strftime('place/%Y/%m/%d/')
    filename_slug = pytils.translit.translify(filename)
    return os.path.join(date_path, filename_slug)


def _create_file_path_for_remotes(filename):
    now = datetime.now()
    date_path = now.strftime('place/%Y/%m/%d/')
    filename_slug = pytils.translit.translify(filename)

    path = os.path.join(settings.MEDIA_ROOT, date_path)
    if not os.path.exists(path):
        os.makedirs(path)

    return os.path.join(date_path, filename_slug)

class Category(models.Model):
    name = models.CharField(
        u'Название категории',
        default='',
        max_length=100)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u'Категория'
        verbose_name_plural = u'Категории'


WITH_WHO = (
    ('any', u'С кем угодно'),
    ('alone', u'Одному'),
    ('family', u'С семьей'),
    ('girlfriend', u'С девушкой-парнем'),
    ('comp', u'С компанией'),
)
class Place(models.Model):
    title = models.CharField(u'Название заведения', max_length=500)

    draft = models.BooleanField(
        u'Опубликован',
        default=False,
        blank=True)

    main_category = models.ManyToManyField(
        Category,
        through='CategoryRelation',
        verbose_name = u'Категории',
        related_name='maincategory_place_set'
    )

    option_category = models.ManyToManyField(
        Category,
        verbose_name = u'Дополнительные опции',
        related_name='option_place_set',
        blank=True
    )

    description = models.TextField(
        u'Краткое описание',
        max_length=140,
        default='',
        blank=True,
        help_text=u'Воувоу!! Наша мегакрутая фишка со 140! символьным описанием заведения')

    addon = models.TextField(
        u'Дополнительная информация',
        max_length=2000,
        default='',
        blank=True)

    with_who = models.CharField(
        u'C кем лучше ходить',
        choices=WITH_WHO,
        default='any',
        blank=True,
        max_length=5)

    # Геоданные #
    address = models.CharField(
        u'Адрес',
        max_length=150,
        blank=True)

    geo_coordinate_lng = models.CharField(
        u'Геокоординаты (широта)',
        max_length=50,
        blank=True)

    geo_coordinate_lat = models.CharField(
        u'Геокоординаты (долгота)',
        max_length=50,
        blank=True)

    # Цены #
    avr_price = models.DecimalField(
        u'Средний чек',
        decimal_places=2,
        max_digits=10,
        default=0,
        blank=True)

    enter_price_man  = models.DecimalField(
        u'Цена за вход для мужчин',
        decimal_places=2,
        max_digits=10,
        default=0,
        blank=True)

    enter_price_woman = models.DecimalField(
        u'Цена за вход для женщин',
        decimal_places=2,
        max_digits=10,
        default=0,
        blank=True)

    # Линки на социальные сети#
    vk_id = models.CharField(
        u'Профиль вконтакте',
        max_length=100,
        default='',
        blank=True)

    twitter_id = models.CharField(
        u'Профиль Twitter',
        max_length=100,
        default='',
        blank=True)

    facebook_id = models.CharField(
        u'Профиль Facebook',
        max_length=100,
        default='',
        blank=True)

    flamp_id = models.CharField(
        u'Профиль Flamp',
        max_length=100,
        default='',
        blank=True)

    foursquare_id = models.CharField(
        u'ID в Foursquare',
        max_length=100,
        default='',
        blank=True)

    double_gis_id = models.CharField(
        u'ID в 2gis',
        max_length=100,
        unique=True)

    def get_street(self):
        try:
            return self.address.split(', ')[0]
        except: return None

    def get_house(self):
        try:
            return self.address.split(', ')[1]
        except: return None

    """ ПОлучить отзывы с флампа """
    def get_flamp_review(self):
        review_url = 'http://flamp.ru/api/2.0/filials/%s/' \
                     'reviews?limit=%s' \
                     '&access_token=%s&depth=2' \
                     '&scopes=user,official_answer,photo' % (
                        self.double_gis_id, str(5), settings.FLAMP_ACCESS_TOKEN
                    )
        req = urllib2.Request(review_url.replace(' ', ''))
        list_reviews = json.loads(urllib2.urlopen(req).read())

        return list_reviews



    """ Методы для получения разных контактных данных """
    def _get_contact_by_type(self, type):
        try:
            return self.contacts.filter(contact_type=type)[0]
        except (IndexError):
            return None

    def get_vk(self):
        return self._get_contact_by_type('vk')

    def get_facebook(self):
        return self._get_contact_by_type('facebook')

    def get_twitter(self):
        return self._get_contact_by_type('twitter')

    def get_foursquare(self):
        return self._get_contact_by_type('foursquare')

    def get_phone(self):
        return self._get_contact_by_type('phone')

    """ Узнаем отдаленность от точки в метрах """
    def distantion(self, point=[56.831718, 60.602092,]):
        return round(geopy_distance(
                point,
                [float(self.geo_coordinate_lat), float(self.geo_coordinate_lng)]
            ).meters / 1000, 2)

    """ Время, до которого работает заведение """
    def works_to(self):
        try:
            d = datetime.today()
            shedule_today = self.workedtimes.filter(
                day=WorkTime.WORKED_TIME_CHOICES[d.weekday()][0])[0]

            # Ставим правильную дату, с которой вычислять время закрытия
            if shedule_today.time_to.hour < shedule_today.time_from.hour:
                d = d + timedelta(days=1)

            # Это время в часах, до которого работает заведение
            if shedule_today.time_to.hour is shedule_today.time_from.hour:
                # Это круглосуточное
                return 1
            else:
                return datetime.combine(d, shedule_today.time_to)
        except (IndexError):
            return None

    """ Получаем самую главную картинку """
    def get_main_image(self):
        try:
            return self.images.get(is_main=True)
        except ItemImage.DoesNotExist:
            if len(self.images.all()) > 0:
                return self.images.all()[0]
            else:
                return None

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return 'place_item', [self.id]

    class Meta:
        verbose_name = u'Заведение'
        verbose_name_plural = u'Заведения'

class DoubleGisTag(models.Model):
    title = models.CharField(u'Имя', max_length=100)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Тег'
        verbose_name_plural = u'Теги'


class DoubleGisCategory(models.Model):
    title = models.CharField(u'Имя', max_length=100)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Категория'
        verbose_name_plural = u'Категории'

class PlaceMeta(models.Model):
    flamp_rate = models.FloatField(
        u'Рейтинг Flamp',
        blank=True,
        default=None,
        null=True)

    flamp_review_count = models.IntegerField(
        u'Количество отзывов на Flamp',
        blank=True,
        default=None,
        null=True)

    flamp_recommendation_count = models.IntegerField(
        u'Количество рекомендаци',
        blank=True,
        default=None,
        null=True)

    # Теги
    double_gis_tags = models.ManyToManyField(
        DoubleGisTag,
        verbose_name=u'Теги 2gis',
        blank=True)

    double_gis_category = models.ManyToManyField(
        DoubleGisCategory,
        verbose_name=u'Категории 2gis',
        blank=True)

    place = models.OneToOneField(Place, verbose_name=u'Заведение', primary_key=True)

    def __unicode__(self):
        return self.pk

    class Meta:
        verbose_name = u'Дополнительное поле'
        verbose_name_plural = u'Дополнительные поля'


class CategoryRelation(models.Model):
    place = models.ForeignKey(Place)
    category = models.ForeignKey(Category)
    order = models.IntegerField(u'Порядок в очереди')


class ItemImage(models.Model):
    place_item = models.ForeignKey(Place, related_name='images')
    image = models.ImageField(u'Изображения', upload_to=_create_file_path)
    #title = models.CharField(u'Название изображения', max_length=500, blank=True)
    is_main = models.BooleanField(u'Главное изображение?', default=False)

    def get_image(self):
        # Проверяем, есть ли картинка
        if not os.path.exists(
                os.path.join(settings.MEDIA_ROOT, self.get_local_filepath())
            ):
            # Скачиваем её
            urlretrieve(
                str(self.image),
                os.path.join(settings.MEDIA_ROOT, self.get_local_filepath()))

            self.image = self.get_local_filepath()
            self.save()

        return self.image

    def get_local_filepath(self):
        if 'http' in str(self.image):
            return _create_file_path_for_remotes('venue_img_%s_%s.jpg' % (
                self.place_item.pk, self.pk))
        else:
            return str(self.image)

    def __unicode__(self):
        return str(self.pk)

    class Meta:
        verbose_name = u'Фото'
        verbose_name_plural = u'Фото'


class WorkTime(models.Model):
    WORKED_TIME_CHOICES = (
        ('Mon', u'Понедельник'),
        ('Tue', u'Вторник'),
        ('Wed', u'Среда'),
        ('Thu', u'Четверг'),
        ('Fri', u'Пятница'),
        ('Sat', u'Суббота'),
        ('Sun', u'Воскресенье'),
        ('any', u'Любой день'),
    )

    place_item = models.ForeignKey(Place, related_name='workedtimes')
    day = models.CharField(u'День',
       choices=WORKED_TIME_CHOICES,
       default='any',
       max_length=3)

    time_from = models.TimeField(u'Время работы от', default='',
       max_length=20)

    time_to = models.TimeField(u'Время работы до', default='',
       max_length=20)

    class Meta:
        verbose_name = u'Время работы'
        verbose_name_plural = u'Время работы'


class Contact(models.Model):
    CONTACT_TYPE = (
        ('', u'---'),
        ('phone', u'Телефон'),
        ('email', u'Эл. почта'),
        ('website', u'Сайт'),
        ('foursquare', u'Foursquare'),
        ('twitter', u'Twitter'),
        ('facebook', u'Facebook'),
        ('vk', u'Вконтакте'),
        ('youtube', u'YouTube'),
    )
    SOCIAL_LIST = {
        'vk': '45688e',
        'facebook': '39599f',
        'twitter': '45b0e3',
        'foursquare': '00aeef',}

    place_item = models.ForeignKey(Place, related_name='contacts')

    contact_type = models.CharField(
        u'Название контакта',
        max_length=100,
        choices=CONTACT_TYPE,
        default=None)

    value = models.CharField(
        u'Значение',
        default='',
        max_length=100)

    class Meta:
        verbose_name = u'Контакт'
        verbose_name_plural = u'Контакты'


class MenuPrice(models.Model):
    MENU_ITEM_LIST = (
        ('bear', u'Пиво'),
        ('rom', u'Ром с колой'),
        ('caes', u'Цезарь'),
        ('coff', u'Чашка кофе'),
    )
    place_item = models.ForeignKey(Place, related_name='menus')
    menu_item = models.CharField(
        u'Блюдо или напиток',
        max_length=5,
        choices=MENU_ITEM_LIST,
        default='bear')

    price = models.DecimalField(
        u'Цена',
        decimal_places=2,
        max_digits=10,
        default=0)

    class Meta:
        verbose_name = u'пункт меню заведения'
        verbose_name_plural = u'Пункты меню заведения'

class Feedback(models.Model):
    content = models.TextField(u'Отзыв', max_length=100)

    def __unicode__(self):
        return self.content

    class Meta:
        verbose_name = u'Отзыв'
        verbose_name_plural = u'Отзывы'