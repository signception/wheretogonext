#-*- coding: UTF-8 -*-
import json
import urllib
import urllib2
from xml.dom import minidom
import time
import random

class TaxiRequest(object):

    taxi_url = 'http://3101010.ru/'
    street_url = 'http://3101010.ru/order/streets.php'
    house_url = 'http://3101010.ru/order/houses.php?street='
    cost_url = 'http://3101010.ru/order/setcost.php'
    order_url = 'http://3101010.ru/order/setorder.php'
    googlemap_url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&sensor=false&language=ru'
    cookie = {}

    def __init__(self, from_coord, to_address, **kwargs):
        # вызываем главную страницу, чтобы поставить куки
        print self.cookie
        self._get_data(self.taxi_url)

        self.from_a = self._get_street(self.get_address_from_coord(from_coord))
        self.to_a = self._get_street(to_address)

        self.from_h = self._get_house(self.from_a,
            self.get_address_from_coord(from_coord)['house'].encode('utf-8'))
        self.to_h =  self._get_house(self.to_a,
            to_address['house'].encode('utf-8'))

        self.price = self._get_cost()

    def request(self, phone, captcha):
        p = {
            'phone': str(phone),
            'name': '',
            'info': '',
            'captcha': str(captcha),
            'remember': 1,
            'buttonOrder': 'Оформить заказ',
            'id': self.uid,
            'uid': random.random()
        }
        self.cookie.update({'order_phone': '89223101010', 'order_name': ''})
        print p
        data = self._get_data(self.order_url, p, 'POST')
        print data.decode('cp1251').encode('utf-8')

    def get_session_id(self):
        if self.cookie:
            return self.cookie['PHPSESSID']
        return None

    def get_address_from_coord(self, coord):
        url = self.googlemap_url % (coord[0], coord[1])
        data = json.loads( self._get_data(url) )
        a = data['results'][0]['address_components']

        return {
            'street': a[1]['short_name'].replace(u'ул. ', ''),
            'house': a[0]['short_name'],
        }

    def _get_data(self, url, data=[], method='GET'):
        header = {
            'User-Agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.77 Safari/537.36',
            'Accept-Charset': 'windows-1251',
        }
        header.update(self._get_cookie())

        data = urllib.urlencode(data)
        req = urllib2.Request(url, data, header)

        req.get_method = lambda: method
        req = urllib2.urlopen(req)

        self._set_cookie(req)
        return req.read()
    
    def _get_street(self, text):
        street = text['street'].encode('utf-8').replace(' ', '')
        street_list = self._get_data(self.street_url, {
            'street': street
        }, 'POST').decode('cp1251').encode('utf-8')
        dom = minidom.parseString(street_list)
        street = dom\
                    .getElementsByTagName('li')[0]\
                    .getAttributeNode('id')\
                    .value
        return street

    def _get_house(self, street, house):
        house = house.encode('utf-8').replace(' ', '')
        house_list = self._get_data(self.house_url + str(street), {
            'house': house
        }, 'POST').decode('cp1251').encode('utf-8')
        dom = minidom.parseString(house_list)
        house = dom\
                    .getElementsByTagName('li')[0]\
                    .getAttributeNode('id')\
                    .value
        return house

    def _get_cost(self):
        params = {
            'address1': str(self.from_h),
            'address2': str(self.to_h),
        }
        self.uid = self._get_data(self.cost_url, params, 'POST').replace('#', '')
        time.sleep(2)
        cost = self._get_data(self.cost_url, {
            'id': str(self.uid)}, 'POST').replace('#', '')
        return cost

    def _set_cookie(self, request=None):
        if request is not None:
            if self.taxi_url in request.geturl():
                c = request.info().get('Set-Cookie')
                if c is not None:
                    c = c.split('; ')
                    for i in c:
                        if self.cookie.get(i.split('=')[0], None) is None:
                            self.cookie.update({
                                i.split('=')[0]: i.split('=')[1]
                            })
        return self.cookie

    def _get_cookie(self):
        if self.cookie:
            c = []
            for i in self.cookie.keys():
                c.append(
                   '%s=%s' % (i, self.cookie[i])
                )
            return {'Cookie': '; '.join(c)}
        return {'Cookie': ''}