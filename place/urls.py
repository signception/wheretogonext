from django.conf.urls import patterns, url

from .models import *
from .views import *

urlpatterns = patterns('',
    url(r'^$', index, name='index'),
    url(r'^company/$', company, name='select_company'),
    url(r'^category/$', category, name='select_category'),
    url(r'^get_venues/$', get_venues, name='get_venues'),

    url(r'^venue/(?P<pk>\d+)/$', PlaceItemDetailView.as_view(), name='place_item'),
    url(r'^get_taxi/(?P<pk>\d+)/$', get_taxi, name='get_taxi'),
    url(r'^get_taxi/(?P<pk>\d+)/(?P<uid>\d+)/$', get_taxi, name='get_taxi_post'),

    url(r'^taxi_success/(?P<pk>\d+)/$', taxi_success, name='taxi_success'),
    url(r'^taxi_failure/(?P<pk>\d+)/$', taxi_failure, name='taxi_failure'),

    url(r'^captcha/(?P<phpsessionid>\w+)/', captcha, name='captcha'),
)
