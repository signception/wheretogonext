#-*- coding: UTF-8 -*-
from django.contrib import admin
from django.contrib.admin.options import TabularInline, StackedInline
from django import forms

from .models import *

def inline_factory(m, parent_class=TabularInline):
    class helper(parent_class):
        model = m
    return helper

# Register your models here.



class FeedbackAdmin(admin.ModelAdmin):
    list_display = ('content',)

class CategoryAdmin(admin.ModelAdmin):
    ordering = ['name',]
    list_display = ('name',)

class PlaceAdmindminForm(forms.ModelForm):
    with_who = forms.MultipleChoiceField(
        choices=WITH_WHO,
        label=u'С кем?',
        widget=forms.CheckboxSelectMultiple(),
        required=False)

    class Meta:
        models = Place


class PlaceAdmin(admin.ModelAdmin):
    inlines = [
        inline_factory(PlaceMeta,parent_class=StackedInline),
        inline_factory(CategoryRelation),
        inline_factory(ItemImage),
        inline_factory(WorkTime),
        inline_factory(Contact),
        inline_factory(MenuPrice),
        #inline_factory(DoubleGisTags),
    ]

    def has_description(self, obj):
        return True if obj.description is not '' else False
    has_description.allow_tags = True
    has_description.boolean = True
    has_description.short_description = u'Описание'

    def flamp_rate(self, obj):
        return obj.placemeta.flamp_rate
    flamp_rate.short_description = u'Рейтинг'

    def categories(self, obj):
        return ', '.join( [i[0] for i in obj.main_category\
                                        .all()\
                                        .values_list('name')])
    categories.short_description = u'Категории'

    def doublegis_category(self, obj):
        return ', '.join( [i[0] for i in obj.placemeta\
                        .double_gis_category\
                        .all()\
                        .values_list('title')])
    doublegis_category.short_description = u'Категории 2gis'

    def doublegis_tags(self, obj):
        return ', '.join( [i[0] for i in obj.placemeta\
                        .double_gis_tags\
                        .all()\
                        .values_list('title')])
    doublegis_tags.short_description = u'Теги 2gis'

    ordering = ['title',]
    list_display = (
        'title',
        'address',
        'avr_price',
        'has_description',
        'flamp_rate',
        'with_who',
        'categories',
        'doublegis_category',
        'doublegis_tags',
        'draft')

    list_editable = ('with_who',)
    list_filter = ('main_category', 'placemeta__double_gis_tags', 'placemeta__double_gis_category')
    form = PlaceAdmindminForm
    search_fields = ('title', 'address', 'foursquare_id', 'double_gis_id')

admin.site.register(Place, PlaceAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Feedback, FeedbackAdmin)