#-*- coding: UTF-8 -*-
import cookielib

import json
import urllib
import urllib2
import time, datetime
import random


PROXI_LIST = [
    {'82.146.54.205': '3128'},
    {'95.26.23.114': '8080'},
    #{'92.126.217.47': '80'},
    #{'94.230.35.108': '80'},
    {'92.255.193.109': '8080'},
    {'149.255.6.67': '8080'},
    {'89.109.52.236': '3128'},
    {'91.191.236.54': '80'},
    {'95.31.132.68': '80'},
]

class TaxiRequest(object):

    taxi_url = 'http://www.300-01-01.ru/'
    street_url = 'http://www.300-01-01.ru/order/streets.php?q=%s&limit=10&timestamp=%s'
    house_url = 'http://www.300-01-01.ru/order/houses.php?q=%s&street=%s&limit=10&timestamp=%s'
    cost_url = 'http://www.300-01-01.ru/order/cost.php'
    order_url = 'http://www.300-01-01.ru/order/order.php'
    googlemap_url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&sensor=false&language=ru'

    cookie = {}
    opener = None
    uid = 0

    def __init__(self):
        self.cookie_handler   = urllib2.HTTPCookieProcessor(cookielib.CookieJar())
        self.redirect_handler = urllib2.HTTPRedirectHandler()
        self.http_handler     = urllib2.HTTPHandler()
        self.https_handler    = urllib2.HTTPSHandler()
 
        # Загружаем хэндлеры в opener
        self.opener = urllib2.build_opener(self.http_handler,
                                           self.https_handler,
                                           self.cookie_handler,
                                           self.redirect_handler)
        headers = {
            'User-Agent' : 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.77 Safari/537.36',
            'Accept-Charset': 'utf8',
            'Accept': 'application/json, text/javascript, */*',
            'Origin': self.taxi_url,
            'Referer': self.taxi_url,
            'Pragma': 'no-cache',
        }

        def dict_to_list(dict):
            dictlist = []
            for key, value in dict.iteritems():
                temp = [key,value]
                dictlist.append(temp)
            return dictlist

        self.opener.addheaders = dict_to_list(headers)

        # Ставим  произвольную проксю из списка
        proxi = PROXI_LIST[random.randrange(0,len(PROXI_LIST)-1)]
        self.proxy_handler = urllib2.ProxyHandler({'https': 'https://%s:%s' % (
                proxi.keys()[0], proxi.values()[0]
            )})
        self.opener.add_handler(self.proxy_handler)

        # вызываем главную страницу, чтобы поставить куки
        self._get_data(self.taxi_url, [], True)

        # Ставим куки
        headers.update(self._get_cookie())
        self.opener.addheaders = dict_to_list(headers)

        urllib2.install_opener(self.opener)
 

    def calculate_cost(self, from_coord, to_address, **kwargs):
        self.from_a = self._get_street(self.get_address_from_coord(from_coord))
        self.to_a = self._get_street(to_address)

        self.from_h = self._get_house(self.from_a,
            self.get_address_from_coord(from_coord)['house'].encode('utf-8'))
        self.to_h =  self._get_house(self.to_a,
            to_address['house'].encode('utf-8'))
        self.price = self._get_cost()

    def request(self, uid, phone, captcha):
        n = datetime.datetime.now()
        p = {
            'phone': str(phone),
            'name': '',
            'date_hour': n.hour,
            'date_minute': n.minute,
            'date_day': n.day,
            'date_month': n.month,
            'date_year': n.year,
            'info': '',
            'smoking': 'yes',
            'captcha': captcha,
            'id': uid,
        }

        data = json.loads(
            self._get_data(self.order_url, p)
        )

        if data.get('id', None) is not None:
            return True
        else:
            return data['error']

    def get_session_id(self):
        if self.cookie:
            return self.cookie['PHPSESSID']
        return None

    def get_address_from_coord(self, coord):
        url = self.googlemap_url % (coord[0], coord[1])
        data = json.loads( self._get_data(url) )
        a = data['results'][0]['address_components']

        return {
            'street': a[1]['short_name'].replace(u'ул. ', ''),
            'house': a[0]['short_name'],
        }

    def _get_data(self, url, data=[], set_cookie=False):
        if data:
            data = urllib.urlencode(data)
            req = urllib2.urlopen(url, data)
        else:
            req = urllib2.urlopen(url)

        if set_cookie:
            self._set_cookie(req)

        return req.read()

    def _get_street(self, text):
        street = text['street'].encode('utf-8')[:24]
        street_data = self._get_data(
            self.street_url % (
                street, int(time.time()))
        )
        try:
            street_list = street_data.split('\n')
            return street_list[0].split('|')[1]
        except (IndexError):
            return False

    def _get_house(self, street, house):
        house = house.replace(' ', '')
        house_data = self._get_data(
            self.house_url % (
                house, street, int(time.time()))
        )
        try:
            house_list = house_data.split('\n')
            return house_list[0].split('|')[1]
        except (IndexError):
            return False

    def _get_cost(self):
        params = {
            'address1': str(self.from_h),
            'address2': str(self.to_h),
        }
        cost = self._get_data(self.cost_url, params)
        cost = json.loads(cost)

        if cost.get('error', None) is not None:
            #print cost.get('error', None)
            return None
        else:
            self.uid = cost['id']
            return cost['cost']

    def _set_cookie(self, request=None):
        if request is not None:
            if self.taxi_url in request.geturl():
                c = request.info().get('Set-Cookie')
                if c is not None:
                    c = c.split('; ')
                    for i in c:
                        if self.cookie.get(i.split('=')[0], None) is None:
                            self.cookie.update({
                                i.split('=')[0]: i.split('=')[1]
                            })

        return self.cookie

    def _get_cookie(self):
        if self.cookie:
            c = []
            for i in self.cookie.keys():
                c.append(
                   '%s=%s' % (i, self.cookie[i])
                )
            return {'Cookie': '; '.join(c) + ';'}
        return {'Cookie': ''}