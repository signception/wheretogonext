rm -rf db.sqlite3
./manage.py syncdb --all --noinput
./manage.py migrate --fake
./manage.py loaddata wheretogonext/fixtures/place.Category.json
./manage.py createsuperuser --email=me@example.com