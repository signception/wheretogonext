#-*- coding: UTF-8 -*-
import os
import foursquare
from django.core.management.base import BaseCommand
from place.models import Place, ItemImage, Contact

CLIENT_ID = '4E4DWP1RPY0NJQRLTFXIKS2RHFV3IRH0OHBOKOGMYNUVCMMV'
CLIENT_SECRET = '3YAMXV4ZIC42D312KHUWD2K4ADKXAPYBKOBFXH4JY2QYJFAF'

class Command(BaseCommand):
    help = u'Подгружаем список заведений с Foursqaure'

    def handle(self, *args, **options):
        client = foursquare.Foursquare(
                    client_id=CLIENT_ID,
                    client_secret=CLIENT_SECRET,
                    redirect_uri='http://fondu.com/oauth/authorize')

        query = [
            'bar',
            'Bar',
            'Dive Bar',
            'drinks',
            'nightlife',
            'coffee',
            'Food',
            'бар',
            'nightclub',
            'клуб',
            'отель',
            'сауна',
            'стриптиз',
            'караоке',
            'кальян',
            'бильярд',
        ]

        cities = [
            'Yekaterinburg'
        ]


        all_venues = []
        for q in query:
            for c in cities:
                venues = client.venues.search(params={
                    'query': q,
                    'near': c,
                    'limit': 50,})

                for v in venues['venues']:
                    if Place.objects.filter(foursquare_id=v['id']).count() == 0:
                        address = v['location']['address'] if v['location'].get('address', None) else ''

                        place = Place(**{
                            'title': v['name'],
                            'address': address,
                            'geo_coordinate_lat': str(v['location']['lat']),
                            'geo_coordinate_lng': str(v['location']['lng']),
                            'foursquare_id': v['id'],
                        })
                        place.save()

                        if len(v['contact']) > 0:
                            try:
                                phone = v['contact']
                                contact = Contact(**{
                                    'place_item': place,
                                    'contact_type': 'phone',
                                    'value': phone
                                })
                                contact.save()
                            except (KeyError):
                                pass

                        photos = client.venues.photos(v['id'], params={})
                        if photos['photos']['count'] > 0:
                            for p in photos['photos']['items']:
                                path = "%swidth%s%s" % (
                                    p['prefix'],
                                    p['width'],
                                    p['suffix'])

                                img = ItemImage(**{
                                    'place_item': place,
                                    'image': path,
                                })
                                img.save()

                        #print client.venues.links(v['id'], params={})
                        #print client.venues.events(v['id'])
                        #print client.venues.hours(v['id'], params={})

                        print "*** Venues %s is ok!" % v['name']

        print 'Thats ok!'