#-*- coding: UTF-8 -*-
import urllib2
import json
import time
from django.conf import settings

from django.core.management.base import BaseCommand
from place.models import  Place, \
                          ItemImage, \
                          Contact, \
                          DoubleGisCategory, \
                          PlaceMeta, \
                          DoubleGisTag, \
                          WorkTime


class Command(BaseCommand):
    help = u'Подгружаем список заведений с 2gis'

    def handle(self, *args, **options):
        WHERE = 'Екатеринбург'

        MARKERS = [
            u'Танцпол',
            u'Живая музыка',
        ]

        WHAT = [
            'бар',
            'ночные%20клубы',
            'ресторан',
            'бильярдные залы',
            'кафе',
            'кофейни',
            'ресторан',
            'кальян-бар',
            'боулинги',
            'сауны',
        ]

        STOP_LIST_CATEGORY = [
            u'Детские игротеки',
            u'Кулинария',
            u'Кондитерские изделия',
            u'Хлебобулочные изделия',
            u'Ледовые дворцы / Катки',
            u'Фитнес-клубы',
            u'Косметические услуги',
            #u'Услуги массажиста',
            #u'Аренда помещений',
            u'Продажа инфракрасных кабин',
            u'Погонажные изделия',
            u'Аксессуары для бань и саун',
            u'Печи / Камины',
            u'Строительство бань / саун',
            u'Городские информационные сайты',
            u'Строительство и монтаж бассейнов / фонтанов',
            u'Лыжные базы / Горнолыжные комплексы',
            u'Прачечные',
            u'SPA-процедуры',
            u'Студии загара',
            u'Реабилитационные центры',
            u'Коррекция веса',
            u'Спортивные секции',
            u'Тренажёрные залы',
            u'Парикмахерские',
            u'Ногтевые студии',
            u'Гостевые дома',
        ]

        LIST_FIELDS = [
            'data.name_parts',
            'data.static_id',
            'data.plus_one',
            'data.contact_groups',
            'data.reviews',
            'data.has_photo',
            'data.booklet.code',
            'data.booklet.main_photo_url',
        ]
        DETAIL_FIELDS = [
            'data.project_id',
            'data.reviews',
            'data.reviews.best_review',
            'data.photos',
            'data.geo.id',
            'data.geo.entrances',
            'data.booklet.code',
            'data.booklet.main_photo_url',
        ]

        # Урлы
        list_url = 'http://catalog.api.2gis.ru/2.0/search?page=%s\
                    &what=%s\
                    &type=filial\
                    &where=%s\
                    &fields=%s\
                    &key=rudcgu3317\
                    &output=json'

        detail_url = 'http://catalog.api.2gis.ru/2.0/details?type=filial\
                    &id=%s\
                    &fields=%s\
                    &key=rudcgu3317\
                    &output=json'

        review_url = 'http://flamp.ru/api/2.0/filials/\
                    %s/reviews?limit=20\
                    &access_token=' + settings.FLAMP_ACCESS_TOKEN + '&depth=1\
                    &scopes=user%2Cofficial_answer%2Cphoto'

        def test_has_in_stop_rubric(data):
            for pr in v['rubrics']['primary']:
                if pr['name'] in STOP_LIST_CATEGORY:
                    try:
                        cat = DoubleGisCategory.objects.get(title=pr['name'])
                        cat.delete()
                    except (DoubleGisCategory.DoesNotExist): pass
                    return True

            for ad in v['rubrics']['additional']:
                if ad['name'] in STOP_LIST_CATEGORY:
                    try:
                        cat = DoubleGisCategory.objects.get(title=pr['name'])
                        cat.delete()
                    except (DoubleGisCategory.DoesNotExist): pass
                    return True

        for tag in WHAT:
            for p in range(1, 8):
                getted_list_url = (list_url % (
                        p, tag, WHERE, ','.join(LIST_FIELDS)
                    )
                )
                getted_list_url = (getted_list_url).replace(' ', '')

                req = urllib2.Request(getted_list_url)
                list_venues = json.loads(urllib2.urlopen(req).read())

                if list_venues.get('result', None) is not None:
                    for venue in list_venues['result']['data']:
                        place, created = Place.objects.get_or_create(double_gis_id=venue['id'].split('_')[0])

                        getted_detail_url = (detail_url % (
                                venue['id'], ','.join(DETAIL_FIELDS)
                            )
                        ).replace(' ', '')
                        req = urllib2.Request(getted_detail_url)

                        # Получаем объект с заведением
                        venue_detail = json.loads(
                            urllib2.urlopen(req).read())

                        v = venue_detail['result']['data'][0]

                        # Если заведение есть в стоп листах, грохаем его нафиг
                        if test_has_in_stop_rubric(v):
                            place.delete()
                            continue

                        # Сохраняем заведение
                        place.title = v['name']
                        place.address = v['geo']['address'] \
                            if v['geo']['address'] is not None else ''

                        place.geo_coordinate_lat = str(v['geo']['lat']) \
                            if str(v['geo']['lat']) is not None else ''

                        place.geo_coordinate_lng = str(v['geo']['lon']) \
                            if str(v['geo']['lon']) is not None else ''

                        place.save()


                        # Добавляем контакты
                        try:
                            for c in v['contact_groups'][0]['contacts']:
                                type = c['type']

                                value = c['value'].split('?')[1] if type in 'website' else c['value']

                                type = 'vk' if 'vk.com' in c['value'] else type
                                type = 'facebook' if 'facebook.com' in c['value'] else type
                                type = 'twitter' if 'twitter.com' in c['value'] else type
                                type = 'youtube' if 'youtube.com' in c['value'] else type
                                type = 'foursquare' if 'foursquare.com' in c['value'] else type

                                if Contact.objects.filter(
                                        place_item=place,
                                        value=value).count() is 0:
                                    contact = Contact(**{
                                        'place_item': place,
                                        'contact_type': type,
                                        'value': value,
                                    })
                                    contact.save()
                        except (IndexError):
                            # Нет контактов
                            pass

                        # Добавляем фоточки
                        for p in v['photos']:
                            pth = p['urls']['original']

                            if ItemImage.objects.filter(
                                    place_item=place,
                                    image=pth).count() is 0:
                                img = ItemImage(**{
                                    'place_item': place,
                                    'image': pth,
                                })
                                img.save()

                        # Добавляем связную модельку
                        try:
                            place_meta = place.placemeta
                        except PlaceMeta.DoesNotExist:
                            place_meta = PlaceMeta(
                                place=place,
                                flamp_rate=v['reviews']['rating'],
                                flamp_review_count=v['reviews']['review_count'],
                                flamp_recommendation_count=v['reviews']['recommendation_count'])
                            place_meta.save()

                        # Добавляем рубрики
                        def save_cat(cat_title, obj):
                            try:
                                cat = DoubleGisCategory.objects.get(title=cat_title)
                            except (DoubleGisCategory.DoesNotExist):
                                cat = DoubleGisCategory(title=cat_title)
                                cat.save()

                            obj.double_gis_category.add(cat)
                            obj.save()

                        for pr in v['rubrics']['primary']:
                            if pr['name'] in STOP_LIST_CATEGORY:
                                place.delete()
                            else:
                                save_cat(pr['name'], place_meta)

                        for ad in v['rubrics']['additional']:
                            save_cat(ad['name'], place_meta)

                        # Добавляем теги
                        def save_tag(cat_title, obj):
                            try:
                                cat = DoubleGisTag.objects.get(title=cat_title)
                            except (DoubleGisTag.DoesNotExist):
                                cat = DoubleGisTag(title=cat_title)
                                cat.save()

                            obj.double_gis_tags.add(cat)
                            obj.save()

                        if v['attributes'].get('food_service', None) is not None:
                            for at in v['attributes']['food_service']['items']:
                                if 'avg_price' in at['name']:
                                    place.avr_price = at['value']
                                    place.save()

                            for m in MARKERS:
                                for at in v['attributes']['food_service']['items']:
                                    try:
                                        if unicode(m) is unicode(at['label']):
                                            save_tag(at['label'], place_meta)

                                        for i in at['label']:
                                            if m in i:
                                                save_tag(i, place_meta)

                                    except (AttributeError, TypeError):
                                        pass

                        # Парсим время работы
                        if v.get('schedule', None) is not None:
                            for d in WorkTime.WORKED_TIME_CHOICES:
                                if WorkTime.objects.filter(
                                        place_item=place,
                                        day=d[0],
                                    ).count() is 0 and v['schedule'].get(d[0], None) is not None:

                                    wh = v['schedule'][d[0]]['working_hours'][0]
                                    work_time = WorkTime(
                                        place_item=place,
                                        day=d[0],
                                        time_from=wh['from'],
                                        time_to=wh['to']
                                    )
                                    work_time.save()

        print 'Thats ok!'